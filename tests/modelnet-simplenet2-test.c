/*
 * Copyright (C) 2013 University of Chicago.
 * See COPYRIGHT notice in top-level directory.
 *
 */


#include <string.h>
#include <assert.h>
#include <ross.h>

#include "codes/model-net.h"
#include "codes/lp-io.h"
#include "codes/codes.h"
#include "codes/codes_mapping.h"
#include "codes/configuration.h"
#include "codes/lp-type-lookup.h"


#define PAYLOAD_SZ 2048 
#define ACK_SZ 500
static int net_id = 0;
static int num_servers;
typedef struct svr_msg svr_msg;
typedef struct svr_state svr_state;

enum svr_event
{
    KICKOFF,  
    ACK,  
    REQ,
    REQ1,    
    LOCAL      
};

struct svr_state{
    int msg_sent_count;   
    int msg_recvd_count;  
    int local_recvd_count; 
    tw_stime start_ts;    
};

struct svr_msg
{
    enum svr_event svr_event_type;
    tw_lpid src;          
    tw_lpid host;
    int incremented_flag; 
};

static void svr_init(
    svr_state * ns,
    tw_lp * lp);
static void svr_event(
    svr_state * ns,
    tw_bf * b,
    svr_msg * m,
    tw_lp * lp);
static void svr_rev_event(
    svr_state * ns,
    tw_bf * b,
    svr_msg * m,
    tw_lp * lp);
static void svr_finalize(
    svr_state * ns,
    tw_lp * lp);

tw_lptype svr_lp = {
    (init_f) svr_init,
    (pre_run_f) NULL,
    (event_f) svr_event,
    (revent_f) svr_rev_event,
    (final_f)  svr_finalize, 
    (map_f) codes_mapping,
    sizeof(svr_state),
};

extern const tw_lptype* svr_get_lp_type();
static void svr_add_lp_type();
static tw_stime ns_to_s(tw_stime ns);
static tw_stime s_to_ns(tw_stime ns);
static void handle_kickoff_event(
    svr_state * ns,
    tw_bf * b,
    svr_msg * m,
    tw_lp * lp);

static void handle_ack_event(
    svr_state * ns,
    tw_bf * b,
    svr_msg * m,
    tw_lp * lp);
    
static void handle_req_event(
    svr_state * ns,
    tw_bf * b,
    svr_msg * m,
    tw_lp * lp);
static void handle_req1_event(
        svr_state *ns,
        tw_bf *b,
        svr_msg *m,
        tw_lp *lp);
static void handle_local_event(
    svr_state * ns,
    tw_bf * b,
    svr_msg * m,
   tw_lp * lp);
static void handle_local_rev_event(
    svr_state * ns,
    tw_bf * b,
    svr_msg * m,
   tw_lp * lp);
static void handle_kickoff_rev_event(
    svr_state * ns,
    tw_bf * b,
    svr_msg * m,
    tw_lp * lp);

static void handle_ack_rev_event(
    svr_state * ns,
    tw_bf * b,
    svr_msg * m,
    tw_lp * lp);
    
static void handle_req_rev_event(
    svr_state * ns,
    tw_bf * b,
    svr_msg * m,
    tw_lp * lp);

const tw_optdef app_opt [] =
{
	TWOPT_GROUP("Model net test case" ),
	TWOPT_END()
};

int main(
    int argc,
    char **argv)
{
    int nprocs;
    int rank;
    int num_nets, *net_ids;
    g_tw_ts_end = s_to_ns(60*60*24*365); 
    lp_io_handle handle;

    tw_opt_add(app_opt);
    tw_init(&argc, &argv);

    if(argc < 2)
    {
	    printf("\n Usage: mpirun <args> --sync=2/3 mapping_file_name.conf (optional --nkp) ");
	    MPI_Finalize();
	    return 0;
    }
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
  
    configuration_load(argv[2], MPI_COMM_WORLD, &config);
    svr_add_lp_type();
    model_net_register();
    
    codes_mapping_setup();

    net_ids = model_net_configure(&num_nets);
    assert(num_nets==1);
    net_id = *net_ids;
    free(net_ids);
        printf("main %d " , num_servers);
    if(lp_io_prepare("modelnet-test", LP_IO_UNIQ_SUFFIX, &handle, MPI_COMM_WORLD) < 0)
    {
        return(-1);
    }

    tw_run();
    printf("FINE!!!");
    model_net_report_stats(net_id);

    if(lp_io_flush(handle, MPI_COMM_WORLD) < 0)
    {
        return(-1);
    }

    tw_end();
    return 0;
}

const tw_lptype* svr_get_lp_type()
{
	    return(&svr_lp);
}

static void svr_add_lp_type()
{
  lp_type_register("server", svr_get_lp_type());
}

static void svr_init(
    svr_state * ns,
    tw_lp * lp){
    tw_event *e;
    svr_msg *m;
    tw_stime kickoff_time;
    memset(ns, 0, sizeof(*ns));
    kickoff_time = g_tw_lookahead + tw_rand_unif(lp->rng); 
    e = codes_event_new(lp->gid, kickoff_time, lp);
    m = tw_event_data(e);
    m->host=0;
    m->svr_event_type = KICKOFF;
    tw_event_send(e);
    return;
}

static void svr_event(
    svr_state * ns,
    tw_bf * b,
    svr_msg * m,
    tw_lp * lp)
{
   switch (m->svr_event_type)
    {
        case REQ:
            handle_req_event(ns, b, m, lp);
            break;
        case ACK:
            handle_ack_event(ns, b, m, lp);
            break;
        case KICKOFF:
            handle_kickoff_event(ns, b, m, lp);
            break;
        case REQ1:
            handle_req1_event(ns,b,m,lp);
            break;
	case LOCAL:
	   handle_local_event(ns, b, m, lp); 
	 break;
        default:
            printf("%d",lp->gid);
	    printf("\n Invalid message type %d ", m->svr_event_type);
            assert(0);
        break;
    }
}

static void svr_rev_event(
    svr_state * ns,
    tw_bf * b,
    svr_msg * m,
    tw_lp * lp)
{
    switch (m->svr_event_type)
    {
        case REQ:
            handle_req_rev_event(ns, b, m, lp);
            break;
        case ACK:
            handle_ack_rev_event(ns, b, m, lp);
            break;
        case KICKOFF:
            handle_kickoff_rev_event(ns, b, m, lp);
            break;
	case LOCAL:
	    handle_local_rev_event(ns, b, m, lp);    
	    break;
        default:
            assert(0);
            break;
    }

    return;
}

static void svr_finalize(
    svr_state * ns,
    tw_lp * lp)
{
    printf("i am here");
    double t = ns_to_s(tw_now(lp) - ns->start_ts);
    printf(" this is t %d ", t);
    printf("server %llu recvd %d bytes in %f seconds, %f MiB/s sent_count %d recvd_count %d local_count %d \n", (unsigned long long)lp->gid, PAYLOAD_SZ*ns->msg_recvd_count, t, 
        ((double)(PAYLOAD_SZ)/(double)(1024*1024)/t), ns->msg_sent_count, ns->msg_recvd_count, ns->local_recvd_count);
    printf("in final");
    return;
}

static tw_stime ns_to_s(tw_stime ns)
{
    return(ns / (1000.0 * 1000.0 * 1000.0));
}

static tw_stime s_to_ns(tw_stime ns)
{
    return(ns * (1000.0 * 1000.0 * 1000.0));
}

static void handle_kickoff_event(
    svr_state * ns,
    tw_bf * b,
    svr_msg * m,
    tw_lp * lp)
{

    svr_msg m_local, m_remote;
    m_local.svr_event_type = LOCAL;
    m_local.src = lp->gid;
    memcpy(&m_remote, &m_local, sizeof(svr_msg));
    m_remote.src = lp->gid;
    m_remote.svr_event_type = ACK;
    ns->start_ts = tw_now(lp);
    /* server 0  sends to 2 */
    /*
    if (lp->gid == 60){
    int dest_id;
    model_net_event(net_id, "test",58,  ACK_SZ, 0.0, sizeof(svr_msg), &m_remote, sizeof(svr_msg), &m_local, lp);
    ns->msg_sent_count++;
    }*/
    if (lp->gid == 62){
    int dest_id;
    model_net_event(net_id, "test",64,  ACK_SZ, 0.0, sizeof(svr_msg), &m_remote, sizeof(svr_msg), &m_local, lp);
    ns->msg_sent_count++;
    }
//    return;
    }
    

static void handle_local_event(
		svr_state * ns,
		tw_bf * b,
		svr_msg * m,
		tw_lp * lp)
{
    ns->local_recvd_count++;
}

static void handle_local_rev_event(
	       svr_state * ns,
	       tw_bf * b,
	       svr_msg * m,
	       tw_lp * lp)
{
   ns->local_recvd_count--;
}
static void handle_req_rev_event(
    svr_state * ns,
    tw_bf * b,
    svr_msg * m,
    tw_lp * lp)
{
    ns->msg_recvd_count--;
    model_net_event_rc(net_id, lp, PAYLOAD_SZ);

    return;
}


/* reverse handler for kickoff */
static void handle_kickoff_rev_event(
    svr_state * ns,
    tw_bf * b,
    svr_msg * m,
    tw_lp * lp)
{
    ns->msg_sent_count--;
    model_net_event_rc(net_id, lp, PAYLOAD_SZ);

    return;
}

static void handle_ack_rev_event(
    svr_state * ns,
    tw_bf * b,
    svr_msg * m,
    tw_lp * lp)
{
    if(m->incremented_flag)
    {
        model_net_event_rc(net_id, lp, PAYLOAD_SZ);
        ns->msg_sent_count--;
    }
    return;
}

static void handle_ack_event(
    svr_state * ns,
    tw_bf * b,
    svr_msg * m,
    tw_lp * lp)
{
    svr_msg m_local;
    svr_msg m_remote;

    m_local.svr_event_type = LOCAL;
    m_local.src = lp->gid;
    int dest_lp;
    memcpy(&m_remote, &m_local, sizeof(svr_msg));
    m_remote.svr_event_type= REQ;
    model_net_event(net_id,"test",68,ACK_SZ,0.0,sizeof(svr_msg),&m_remote,sizeof(svr_msg),&m_local,lp);
        ns->msg_sent_count++;
}
static void handle_req_event(
    svr_state * ns,
    tw_bf * b,
    svr_msg * m,
    tw_lp * lp)
{
    printf("IN REQ");
    svr_msg m_remote , m_local;
    m_local.svr_event_type = LOCAL;
    m_local.src = lp->gid;
    memcpy(&m_remote, &m_local, sizeof(svr_msg));
    m_remote.svr_event_type = REQ1;
    m_remote.src = lp->gid;
   // ns->msg_recvd_count++;
    ns->msg_recvd_count++;
    // will need an incremented flag for this
    ns->msg_sent_count++;
    printf("here");
    model_net_event(net_id, "test", 66, ACK_SZ, 0.0, sizeof(svr_msg), &m_remote, sizeof(svr_msg), &m_local, lp);
    printf("HERE");}
static void handle_req1_event(
    svr_state * ns,
    tw_bf * b,
    svr_msg * m,
    tw_lp * lp)
{
    svr_msg m_local;
    svr_msg m_remote;
    m_local.svr_event_type = LOCAL;
    m_local.src = lp->gid;
    memcpy(&m_remote, &m_local, sizeof(svr_msg));
    m_remote.svr_event_type = ACK;
    m_remote.src = lp->gid;
   // ns->msg_recvd_count++;
    ns->msg_recvd_count++;
    // will need an incremented flag for this
    ns->msg_sent_count++;
    model_net_event(net_id, "test", 60, ACK_SZ, 0.0, sizeof(svr_msg), &m_remote, sizeof(svr_msg), &m_local, lp);
}

/*
 * Local variables:
 A*  c-indent-level: 4
 *  c-basic-offset: 4
 * End:
 *
 * vim: ft=c ts=8 sts=4 sw=4 expandtab
 */
