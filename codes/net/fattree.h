
#ifndef FATTREE_H
#define FATTREE_H
typedef struct ft_message  ft_message;
enum ft_event_type{
	send_up,
	send_down,
	S_send_up,
	S_send_down
};
struct ft_message{
	enum ft_event_type type;
	tw_lpid src_gid;
	tw_lpid final_dest_gid;
	uint64_t net_msg_size_bytes;
	int event_size_bytes;
	int local_event_size_bytes;
	tw_stime next_available_time_saved;
	int magic;
	tw_stime send_time_saved;
	void * temp_event;
	tw_stime recv_time_saved;
	tw_stime travel_start_time;
	//hardcoded for now
    	char category[20];
};
#endif

