/*
 * Copyright (C) 2015 Illinois Institute of Technology.
 * See COPYRIGHT notice in top-level directory.
 * author @Adnan Haider
 */

#include <string.h>
#include <assert.h>
#include <ross.h>
#include "codes/net/fattree.h"

#include "codes/lp-io.h"
#include "codes/jenkins-hash.h"
#include "codes/model-net-method.h"
#include "codes/model-net.h"
#include "codes/model-net-lp.h"
#include "codes/codes_mapping.h"
#include "codes/codes.h"
#include "codes/net/fattree.h"
#define CATEGORY_NAME_MAX 16
#define CATEGORY_MAX 12

#define LP_CONFIG_NM (model_net_lp_config_names[FATTREE])
#define LP_METHOD_NM (model_net_method_names[FATTREE])
typedef struct ft_state ft_state;
typedef struct link_state link_state;
typedef struct switch_state switch_state;
struct link_state
{
    int busy;
    tw_stime next_available_time;
    int dest_node;
};
int power( int  n ,int base){
    if ( n == 1 ) return base;
    return power( (n - 1), base) * base;}
void min( int j , link_state* ls)
{
    int i;
    //need to change later
    int temp = ls[0].next_available_time;

    for (i = 0; i < j; i ++) if ( ls[i].next_available_time < temp) temp = i;
    return temp;
}
typedef struct fattree_param fattree_param;
struct fattree_param
{
    int net_startup_ns; 
    int net_bw_mbps; 
 //m = num ports / links
    int m;
    // n = levels -1
    int n;
    int edge_bandwidth;
    int core_bandwidth;
    // derived paramaeters
    int row_num;
    int num_cores;
    int total;
    int switch_in_pod;
    int num_switches;
    int routing;
};
//will be used for second lp
struct switch_state
{
    link_state *links;
    link_state *upLinks;
    //is core switch or an intermediate switch
    int core_switch;
    int intm_switch;
    char *anno;
    int node_id;
    fattree_param params;
    struct mn_stats ft_stats_array[CATEGORY_MAX];
};
struct ft_state
{
    /* next idle times for network card, both inbound and outbound */
    //unsigned int pod_id;
    link_state *links;
    link_state *upLinks;
    //look up parameters done use  currently
    //what type of node
    int processing_node;
    int core_node;
    int switch_node;
    char * anno;
    //for routing
    fattree_param params;
    int node_id;
    struct mn_stats ft_stats_array[CATEGORY_MAX];
};
enum ROUTING_ALGO
{
    GREEDY=0,
    ECMP
};

static uint64_t                  num_params = 0;
static fattree_param         * all_params = NULL;
static const config_anno_map_t * anno_map   = NULL;
const char lp_group_name[MAX_NAME_LENGTH];
const char lp_type_name[MAX_NAME_LENGTH];
int mapping_rep; 
int mapping_offset;
static int ft_magic = 0;

static const tw_lptype* sn_get_lp_type(void);
static int ft_get_magic();
static int sn_get_msg_sz(void);


#if SIMPLENET_DEBUG
static void print_msg(sn_message *m);
#endif

/* collective network calls */
static void fattree_collective();

/* collective network calls-- rc */
static void fattree_collective_rc();
static void ft_configure();

/* Issues a simplenet packet event call */
static tw_stime fattree_packet_event(
     char* category, 
     tw_lpid final_dest_lp, 
     uint64_t packet_size, 
     int is_pull,
     uint64_t pull_size, /* only used when is_pull==1 */
     tw_stime offset,
     const mn_sched_params *sched_params,
     int remote_event_size, 
     const void* remote_event, 
     int self_event_size,
     const void* self_event,
     tw_lpid src_lp,
     tw_lp *sender,
     int is_last_pckt);
static void fattree_net_packet_event_rc(tw_lp *sender);

static void fatree_net_packet_event_rc(tw_lp *sender);
static void handle_send_down(ft_state*, tw_bf* , ft_message*, tw_lp*);
static void handle_send_up(ft_state*,tw_bf*, ft_message*, tw_lp*);
static void handle_S_send_up(switch_state*, tw_bf*, ft_message*, tw_lp*);
static void handle_S_send_down(switch_state*, tw_bf*, ft_message*, tw_lp*);
static void ft_report_stats();

static tw_lpid ft_find_local_device(
        const char * annotation, 
        int          ignore_annotations,
        tw_lp      * sender);
static void fattree_packet_event_rc(tw_lp *sender)
{
    codes_local_latency_reverse(sender);
    return;
}


static void ft_init(
    ft_state * ns,
    tw_lp * lp);
static void switch_init(
        switch_state *s ,
        tw_lp *lp);
static void ft_event(
    ft_state * ns,
    tw_bf * b,
    ft_message * m,
    tw_lp * lp);
static void ft_switch_event(
        switch_state * s,
        tw_bf * b,
        ft_message * m,
        tw_lp *lp);
static void ft_rev_event(
    ft_state * ns,
    tw_bf * b,
    ft_message * m,
    tw_lp * lp);
static void ft_switch_rev_event(
        switch_state * s,
        tw_bf * b,
        ft_message *m,
        tw_lp *lp);
static void ft_finalize(
    ft_state * ns,
    tw_lp * lp);

tw_lptype ft_lps[] = 
{
    {
        (init_f) ft_init,
        (pre_run_f) NULL,
        (event_f) ft_event,
        (revent_f) ft_rev_event,
        (final_f) ft_finalize,
        (map_f) codes_mapping,
        sizeof(ft_state)
    },
    {
        (init_f) switch_init,
        (pre_run_f) NULL,
        (event_f) ft_switch_event,
        (revent_f) ft_switch_rev_event,
        (final_f) ft_finalize,
        (map_f) codes_mapping,
        sizeof(switch_state)
    },
    {0},
};

static tw_stime rate_to_ns(uint64_t bytes, double MB_p_s);
static void handle_msg_ready_rev_event(
    ft_state * ns,
    tw_bf * b,
    ft_message * m,
    tw_lp * lp);
static void handle_msg_ready_event(
    ft_state * ns,
    tw_bf * b,
    ft_message * m,
    tw_lp * lp);
static void handle_msg_start_rev_event(
    ft_state * ns,
    tw_bf * b,
    ft_message * m,
    tw_lp * lp);
static void handle_msg_start_event(
    ft_state * ns,
    tw_bf * b,
    ft_message * m,
    tw_lp * lp);

static const tw_lptype* ft_get_lp_type()
{
    return(&ft_lps[0]);
}
static const tw_lptype* ft_switch_get_lp_type()
{
    return(&ft_lps[1]);
}

static int ft_get_msg_sz(void)
{
    return(sizeof(ft_message));
}
static void fattree_collective()
{
/* collectives not supported */
    return;
}

static void fattree_collective_rc()
{
/* collectives not supported */
   return;
}

static void ft_report_stats()
{
   return;
}
struct model_net_method fattree_method =
{
    .mn_configure = ft_configure,
    .model_net_method_packet_event = fattree_packet_event,
    .model_net_method_packet_event_rc = fattree_packet_event_rc,
    .model_net_method_recv_msg_event = NULL,
    .model_net_method_recv_msg_event_rc = NULL,
    .mn_get_lp_type = ft_get_lp_type,
    .mn_get_msg_sz = ft_get_msg_sz,
    .mn_report_stats = ft_report_stats,
    .model_net_method_find_local_device = ft_find_local_device,
    .mn_collective_call = fattree_collective,	
    .mn_collective_call_rc = fattree_collective_rc
};
static void ft_init(
    ft_state * ns,
    tw_lp * lp)
{
   uint32_t h1 = 0, h2 = 0;
    memset(ns, 0, sizeof(ft_state));
    // gets all parameters
    ns->anno = codes_mapping_get_annotation_by_lpid(lp->gid);
    if (ns->anno == NULL){
        ns->params = all_params[num_params-1];
    }
    else{
        int id = configuration_get_annotation_index(ns->anno, anno_map);
        ns->params = all_params[id];
    }
    //relative id - when two different lps we will have to sets of relative_ids
    //allocate memory
    ns->node_id = codes_mapping_get_lp_relative_id(lp->gid,0, 1);
    ns->links= (link_state*)malloc(2 * sizeof(link_state));
    ns->upLinks= (link_state*)malloc(2 * sizeof(link_state));
    //this is the default- one of these will change to 1
    //get all parameters that are necessary
    ns->processing_node =0;
    ns->core_node = 0;
    ns->switch_node=0;
    
    int m = ((ns->params).m);
    int n = ((ns->params).n);
    int row_num = ns->params.row_num;
    int switch_in_pod = ns->params.switch_in_pod;
    int num_cores = ns->params.num_cores;
    int num_switches= ns->params.num_switches;
    int total = ns->params.total;
    //determine what kind of node
    int last_row = total - row_num;
    (ns->upLinks)[0].busy = 0;
    (ns->upLinks)[0].next_available_time= tw_now(lp);
    (ns->upLinks)[0].dest_node= (ns->node_id/(m/2)) + last_row ;
    
      bj_hashlittle2(LP_METHOD_NM, strlen(LP_METHOD_NM), &h1, &h2);
      ft_magic = h1+h2;
    // testing for upLInks

    return;
}
static void switch_init(switch_state * s, tw_lp * lp)
{

    uint32_t h1 = 0, h2 = 0;
    memset(s, 0, sizeof(switch_state));
    // gets all parameters
    s->anno = codes_mapping_get_annotation_by_lpid(lp->gid);
    if (s->anno == NULL){
        s->params = all_params[num_params-1];
    }
    else{
        int id = configuration_get_annotation_index(s->anno, anno_map);
        s->params = all_params[id];
    }
    //relative id - when two different lps we will have to sets of relative_ids
    //allocate memory
    s->node_id = codes_mapping_get_lp_relative_id(lp->gid,0, 1);
    s->links= (link_state*)malloc(((s->params).m)* sizeof(link_state));
    s->upLinks= (link_state*)malloc(((s->params).m)* sizeof(link_state));
    //this is the default- one of these will change to 1
    s->core_switch=0;
    s->intm_switch=0;
    //get all parameters that are necessary
    int m = s->params.m;
    int n = s->params.n;
    int row_num = s->params.row_num;
    int switch_in_pod = s->params.switch_in_pod;
    int num_cores = s->params.num_cores;
    int num_switches= s->params.num_switches;
    int total = s->params.total;
    //determine what kind of node
    if (s->node_id >= num_cores) s->intm_switch = 1;
    else s->core_switch = 1;
    int i , offset;
    if (s->core_switch)
    {
     //core node init
     //loop through each port and set link values
     for(i = 0; i < m; i ++)
     {
       offset = (s->node_id)/(m/2);
      (s->links)[i].busy = 0;
      (s->links)[i].next_available_time= tw_now(lp);
      //will attach each link to a diffrent pod
      (s->links)[i].dest_node= num_cores + (switch_in_pod * i) + offset;
      //don't need to set upLinks as there are not any for the core switches
     }
    }
    else
    {
        for(i =0; i< (m/2); i ++)
        {
            offset = (s->node_id - num_cores) % (m/2);
            if (s->node_id + row_num < total  && s->node_id - row_num >= num_cores)
            {
                //connects to switch node on both upLinks and links -> in a middle layer for the intermediate switches
                (s->links)[i].busy = 0;
                (s->links)[i].next_available_time= tw_now(lp);
                //should equal relative id of the switch below
                (s->links)[i].dest_node= s->node_id - offset + i + row_num;
                (s->upLinks)[i].busy=0;
                (s->upLinks)[i].next_available_time=tw_now(lp);
                //should equal relative id of the switch above
                (s->upLinks)[i].dest_node= s->node_id - offset - row_num + i;
            }
            else if ( s->node_id - row_num >= num_cores) 
            {
                //at the bottom most layer
                //switch's position with respect to the last row = offset1
                int offset1= s->node_id - num_cores - ((n-2) * row_num) ;
                (s->upLinks)[i].busy=0;
                (s->upLinks)[i].next_available_time=tw_now(lp);
                (s->upLinks)[i].dest_node= s->node_id - offset - row_num + i;
                (s->links)[i].busy=0;
                (s->links)[i].next_available_time=tw_now(lp);
                //relative id of processing node
                (s->links)[i].dest_node = ((m/2) * offset1) + i;
            }
            else if (s->node_id + row_num < total)
            {
                //at the top most layer
                (s->links)[i].busy = 0;
                (s->links)[i].next_available_time= tw_now(lp);
                (s->links)[i].dest_node= s->node_id - offset + i + row_num;
                (s->upLinks)[i].busy = 0;
                (s->upLinks)[i].next_available_time= tw_now(lp);
                //temp fix
                if (row_num == m) (s->upLinks)[i].dest_node = i;
                else
                //equals the core node relative id
                (s->upLinks)[i].dest_node= (offset * (m/2)) + i;
            }
            //only one switch layer
            else 
            {
                offset = s->node_id - num_cores;
                (s->links)[i].busy = 0;
                (s->links)[i].next_available_time= tw_now(lp);
                // offset aware
                (s->links)[i].dest_node= ((m/2) * offset) + i;
                (s->upLinks)[i].busy = 0;
                (s->upLinks)[i].next_available_time= tw_now(lp);
                // one to one map
                (s->upLinks)[i].dest_node= i;
            }
        }}
        return;
}
tw_lpid get_next_down_ecmp( switch_state *ns, tw_lp *lp, tw_lpid dest, tw_stime *time, uint64_t bytes)
{
    // essentially the same as greedy approach -> will need to change as we scale with more features
    int dummy;
    int n = ((ns->params).n);
    int m = (ns->params).m;
    int row_num = ns->params.row_num;
    int core_bandwidth = (ns->params).core_bandwidth;
    int edge_bandwidth = (ns->params).edge_bandwidth;
    int indv_offset = row_num/m;
    int num_cores = ns->params.num_cores;
    int total = ns->params.total;
    int switch_in_pod = ns->params.switch_in_pod;
    int num_switches = ns->params.num_switches;
    //gets relative id of processing node 0...N-1
    int seek_node = codes_mapping_get_lp_relative_id(dest,0,1);
    codes_mapping_get_lp_info( lp->gid, lp_group_name,&dummy,lp_type_name,&dummy, ns->anno, &mapping_rep, &mapping_offset);
    //gets the offset of processing node with respect to other processing nodes - will need to change once add second lp type
    //must already be at switch node  intermediary layer
    if(ns->node_id >= (num_switches - row_num) )
    {
        //find processing node and were done!
        int j ;
        if(ns->node_id == 12 && dest ==3) return dest;
        if(ns->node_id == 18 && dest == 42) return dest;
        if(ns->node_id == 16 && dest ==29) return dest;
        if(ns->node_id == 14 && dest == 16) return dest;
        for (j = 0; j <m/2; j++)
        {

            // there are not multi routes since each compute node has one upLink
            if (ns->links[j].dest_node == seek_node) 
            {
                if( tw_now(lp) < ns->links[j].next_available_time)
                {   
                    *time += (ns->links[j].next_available_time - tw_now(lp));
                }
                else 
                {
                    ns->links[j].next_available_time = tw_now(lp);
                }
                //since we are between switch and compute node
                *time += rate_to_ns(bytes, edge_bandwidth);
                //does it matter if this is less than tw_now(lp)?
                ns->links[j].next_available_time += rate_to_ns(bytes,edge_bandwidth);
                // return codes_mapping_get_lpid_from_relative((ns->links)[j].dest_node,lp_group_name,lp_type_name,ns->anno,0);
                
                return dest;
            }
            
        }
    }
    else 
    {
        int offset = ns->node_id - num_cores;
        offset = offset % row_num;
        int divider = (m/2) * offset;
        int i ;
        // finds which intermediary switch to send to -> only one possible route for now
        for ( i =1; i <= m/2 ; i ++){
            if( seek_node < divider + (m/2)*i)
            {
                if (tw_now(lp) < ns->links[i-1].next_available_time)
                {
                    *time += ns->links[i-1].next_available_time - tw_now(lp);
                }
                else
                {
                    ns->links[i-1].next_available_time = tw_now(lp);
                }
                //since this is between switches
                *time += rate_to_ns(bytes, core_bandwidth);
                ns->links[i-1].next_available_time += rate_to_ns(bytes,core_bandwidth);
                return codes_mapping_get_lpid_from_relative((ns->links)[i-1].dest_node,lp_group_name,"fattree_switch",ns->anno,0);
            }
        }
    }
}

//determine whether to send up or to send down- or 
//time = recv_queue_time
tw_lpid get_down_stop(ft_state *ns, tw_lp * lp, tw_lpid dest, tw_stime *time, uint64_t bytes)
{
    int dummy;
    int n = ((ns->params).n);
    int m = (ns->params).m;
    int row_num = ns->params.row_num;
    int core_bandwidth = (ns->params).core_bandwidth;
    int edge_bandwidth = (ns->params).edge_bandwidth;
    int indv_offset = row_num/m;
    int num_cores = ns->params.num_cores;
    int total = ns->params.total;
    int switch_in_pod = ns->params.switch_in_pod;
    int num_switches = ns->params.num_switches;
    //gets relative id of processing node 0...N-1
    int seek_node = codes_mapping_get_lp_relative_id(dest,0,1);
    codes_mapping_get_lp_info( lp->gid, lp_group_name,&dummy,lp_type_name,&dummy, ns->anno, &mapping_rep, &mapping_offset);
    //gets the offset of processing node with respect to other processing nodes - will need to change once add second lp type
    int rel_node = seek_node - num_switches;
    //must already be at switch node  intermediary layer
    if(ns->links[0].dest_node >= num_switches)
    {
        //find processing node and were done!
        int j ;
        for (j = 0; j <m/2; j++)
        {
            if (ns->links[j].dest_node == seek_node) 
            {
                if( tw_now(lp) < ns->links[j].next_available_time)
                {
                    *time += (ns->links[j].next_available_time - tw_now(lp));
                }
                else 
                {
                    ns->links[j].next_available_time = tw_now(lp);
                }
                //since we are between switch and compute node
                *time += rate_to_ns(bytes, edge_bandwidth);
                //does it matter if this is less than tw_now(lp)?
                ns->links[j].next_available_time += rate_to_ns(bytes,edge_bandwidth);
                // return codes_mapping_get_lpid_from_relative((ns->links)[j].dest_node,lp_group_name,lp_type_name,ns->anno,0);
                return dest;
               // return dest;
            }
        }
    }
    else 
    {
        int offset = ns->node_id - num_cores;
        int offset1 = offset % switch_in_pod;
        int min_node = (offset - offset1) * m/2;
        int i ;
        for ( i =1; i <= m/2 ; i ++){
            if( rel_node < min_node + (m/2)*i)
            {
                if (tw_now(lp) < ns->links[i-1].next_available_time)
                {
                    *time = (tw_now(lp) - ns->links[i-1].next_available_time);
                }
                else
                {
                    ns->links[i-1].next_available_time = tw_now(lp);
                }
                //since this is between switches
                *time += rate_to_ns(bytes, core_bandwidth);
                ns->links[i-1].next_available_time += rate_to_ns(bytes,core_bandwidth);
                return codes_mapping_get_lpid_from_relative((ns->links)[i-1].dest_node,lp_group_name,lp_type_name,ns->anno,0);
            }
        }
    }
}
tw_lpid get_next_up_ecmp(switch_state *ns, tw_lp* lp, tw_lpid dest, tw_stime* time, uint64_t bytes, int* flag)
{

    // decides which link to use using ecmp algorithm
    int dummy;
    int n = ((ns->params).n);
    int m = (ns->params).m;
    int core_bandwidth = (ns->params).core_bandwidth;
    int edge_bandwidth = (ns->params).edge_bandwidth;
    int startup = (ns->params).net_startup_ns;
    int row_num = ns->params.row_num;
    int indv_offset = row_num/m;
    int num_cores = ns->params.num_cores;
    int total = ns->params.total;
    int switch_in_pod = ns->params.switch_in_pod;
    int num_switches = ns->params.num_switches;
    int seek_node = codes_mapping_get_lp_relative_id(dest,0,1);
    codes_mapping_get_lp_info( lp->gid, lp_group_name,&dummy,lp_type_name,&dummy, ns->anno, &mapping_rep, &mapping_offset);

        if (((ns->upLinks)[0].dest_node) >= num_cores)
        {
            //not at top layer keep going forward
            int temp=0;
            int i = 0;
            //pick a random upLink from 0 to m/2
            temp = tw_rand_integer(lp->rng, 0 , ((m/2)-1));
            if ( ns->upLinks[temp].next_available_time > tw_now(lp) )
            {
             //   printf("QUEUE EFFECT3 %f " , ns->upLinks[temp].next_available_time - tw_now(lp));
                *time += ns->upLinks[temp].next_available_time - tw_now(lp);
            }
            else 
            {
                ns->upLinks[temp].next_available_time = tw_now(lp);
            }

            //since we are moving between switches we use core_bandwidth
            *time += rate_to_ns(bytes, core_bandwidth);
            ns->upLinks[temp].next_available_time += rate_to_ns(bytes,core_bandwidth);
                
            return codes_mapping_get_lpid_from_relative(ns->upLinks[temp].dest_node,lp_group_name, "fattree_switch", ns->anno,0);
        }
        else if ( ns->node_id < num_cores)
        {
        //at core layer
        int num_processing = (m/2) * switch_in_pod;
        int j ;
        int temp ;
        for ( j =1 ; j <= m; j ++)
        {
           if( seek_node < (num_processing * j)) 
           {
            tw_stime rate = rate_to_ns(bytes, core_bandwidth);
            if(tw_now(lp) < ns->links[j-1].next_available_time)
            {

                *time += (ns->links[j-1].next_available_time)- tw_now(lp);
            }
            else 
            {
                ns->links[j-1].next_available_time = tw_now(lp);
            }   
            *time += rate;
            ns->links[j-1].next_available_time += rate;
            *flag = 1;
            return codes_mapping_get_lpid_from_relative((ns->links[j-1].dest_node),lp_group_name, "fattree_switch", ns->anno, 0);
            }
        }
        }
        else 
        {
            //at top
           //get param values 
           int rel_id = ns->node_id - num_cores;
           int offset = rel_id / switch_in_pod;
           int divider = offset * (switch_in_pod * (m/2));
           int dividermax = divider + (switch_in_pod * (m/2));
           //inclusive
           //exclusive
           //calculates relative id for dest lp processsing node- in future need to make another lp
           
           int i = 0;
         
           if (seek_node < divider || seek_node >= dividermax)
           {
                //must go to the core switches
                int temp = 0;
                tw_stime min = ns->upLinks[i].next_available_time;
                temp = tw_rand_integer(lp->rng, 0 , (m/2)-1 );
                if ( tw_now(lp) < ns->upLinks[temp].next_available_time)
                {
                    *time = (ns->upLinks[temp].next_available_time) - tw_now(lp);
                }
                else
                {
                    ns->upLinks[temp].next_available_time = tw_now(lp);
                }
                //we are going in between switches thus we use the higher bandwidth
                tw_stime rate = rate_to_ns(bytes,core_bandwidth);
                ns->upLinks[temp].next_available_time += rate;
                *time += rate;
                
            return codes_mapping_get_lpid_from_relative((ns->upLinks[temp].dest_node),lp_group_name, "fattree_switch", ns->anno,0);
           }

           // we must send down from the first intermediate switch
           // procesing node is in current pod
           // no multipath options 
           int j ;
           for ( j = 1; j <= m/2 ; j++)
           {
               if ( seek_node < divider + (j * (m/2)))
               {
                   //calculates time
                   tw_stime rate = rate_to_ns(bytes, core_bandwidth);
                    if(tw_now(lp) < (ns->links)[j-1].next_available_time)
                    {
                        *time += (ns->links[j-1].next_available_time)- tw_now(lp);
                    }
                    else 
                    {
                        ns->links[j-1].next_available_time = tw_now(lp);
                    }   
                    *time += rate;
                    ns->links[j-1].next_available_time += rate;

                   //also flag doing send_down event
                   *flag = 1;
                   if(n == 2)// return codes_mapping_get_lpid_from_relative((ns->links[j-1].dest_node),lp_group_name,"modelnet_fattree",ns->anno,0);
                        return lp->gid;
                   

                   return codes_mapping_get_lpid_from_relative((ns->links[j-1].dest_node), lp_group_name,"fattree_switch",ns->anno,0);
                   
               }
           }
    }}

tw_lpid get_stop(ft_state *ns, tw_lp * lp , tw_lpid dest, tw_stime * time, uint64_t bytes)
{
    //how decides which link to use need routing algorithm - greedy approach
    
    // will need to allocate the values 
    //is this annotation equal to prev annotation?
    int dummy;
    int n = ((ns->params).n);
    int m = (ns->params).m;
    int core_bandwidth = (ns->params).core_bandwidth;
    int edge_bandwidth = (ns->params).edge_bandwidth;
    int row_num = ns->params.row_num;
    int indv_offset = row_num/m;
    int num_cores = ns->params.num_cores;
    int total = ns->params.total;
    int switch_in_pod = ns->params.switch_in_pod;
    int num_switches = ns->params.num_switches;
    int seek_node = codes_mapping_get_lp_relative_id(dest,0,1);
    codes_mapping_get_lp_info( lp->gid, lp_group_name,&dummy,lp_type_name,&dummy, ns->anno, &mapping_rep, &mapping_offset);
    int rel_node;
    rel_node = seek_node - num_switches;
    if (ns->processing_node)
    {
        if (tw_now(lp) < (ns->upLinks[0].next_available_time))
        {
            printf("here");
            *time += (ns->upLinks[0].next_available_time) - tw_now(lp);
        }
        else 
        {
            //since the next available time must be the same as the lp time before issuing packet
            ns->upLinks[0].next_available_time = tw_now(lp);
        }
        //edge bandwidth becuase between switch and compute
        *time += rate_to_ns(bytes , edge_bandwidth);
        ns->upLinks[0].next_available_time += rate_to_ns(bytes,edge_bandwidth);
        return codes_mapping_get_lpid_from_relative((ns->upLinks)[0].dest_node,lp_group_name,lp_type_name,ns->anno,0);
    }
    else if (ns->switch_node)
    {
        if (((ns->upLinks)[0].dest_node) >= num_cores)
        {
            //not at top layer keep going forward
            int temp=0;
            int i = 0;
            tw_stime min = ns->upLinks[i].next_available_time;
            //find upLink with the shortest next available time
            for ( i = 0; i < m/2; i ++)
            {
                if((ns->upLinks)[i].next_available_time < min)
                {
                    min = (ns->upLinks[i].next_available_time);
                    *time = (ns->upLinks[i].next_available_time);
                    temp = i;
                }
            }
            if (tw_now(lp) < ns->upLinks[temp].next_available_time)
            {
                printf("here1");
                *time += (ns->upLinks[temp].next_available_time) - tw_now(lp);
            }
            else 
            {
                ns->upLinks[temp].next_available_time = tw_now(lp);
            }
            //since we are moving between switches we use core_bandwidth
            *time += rate_to_ns(bytes, core_bandwidth);
            ns->upLinks[temp].next_available_time += rate_to_ns(bytes,core_bandwidth);
            return codes_mapping_get_lpid_from_relative(ns->upLinks[temp].dest_node,lp_group_name, lp_type_name, ns->anno,0);
        }
        else
        {
            //at top
           //get param values 
           int rel_id = ns->node_id - num_cores;
           int offset = rel_id % switch_in_pod;
           int max_offset = rel_id + (switch_in_pod - offset);
           int min_offset = rel_id - offset;
           //inclusive
           int min_node = (m/2) * min_offset;
           //exclusive
           int max_node= (m/2)* max_offset;
           //calculates relative id for dest lp processsing node- in future need to make another lp
           
           int i = 0;
         
           if (rel_node < min_node)
           {
                //must go to the core switches
                int temp = 0;
                tw_stime min = ns->upLinks[i].next_available_time;
                for ( i = 0; i < m/2; i ++)
                {
                    if((ns->upLinks)[i].next_available_time < min)
                    {
                        min = (ns->upLinks)[i].next_available_time;
                        temp = i;
                    }

                }
                if ( tw_now(lp) < ns->upLinks[temp].next_available_time)
                {
                    printf("here3");
                    *time = (ns->upLinks[temp].next_available_time) - tw_now(lp);
                }
                else
                {
                    ns->upLinks[temp].next_available_time = tw_now(lp);
                }
                //we are going in between switches
                tw_stime rate = rate_to_ns(bytes,core_bandwidth);
                ns->upLinks[temp].next_available_time += rate;
                *time += rate;
            return codes_mapping_get_lpid_from_relative((ns->upLinks[temp].dest_node),lp_group_name, lp_type_name, ns->anno,0);
           }

           if (rel_node >= max_node)
           {
                //must go to the core switches
                int temp = 0;
                tw_stime min = ns->upLinks[i].next_available_time;
                for ( i = 0; i < m/2; i ++)
                {
                    if((ns->upLinks)[i].next_available_time < min)
                    {
                        *time = (ns->upLinks[i].next_available_time);
                        min = (ns->upLinks)[i].next_available_time;
                        temp = i;
                    }
                }
                tw_stime  rate = rate_to_ns(bytes, core_bandwidth);
                if(tw_now(lp) < ns->upLinks[temp].next_available_time)
                {
                    printf("here3");
                    *time += (ns->upLinks[temp].next_available_time)- tw_now(lp);
                }
                else 
                {
                    ns->upLinks[temp].next_available_time = tw_now(lp);
                }
                *time +=rate;
                ns->upLinks[temp].next_available_time += rate;
            return codes_mapping_get_lpid_from_relative((ns->upLinks[temp].dest_node),lp_group_name, lp_type_name, ns->anno,0);
            }
           // we must send down from the first intermediate switch
           int j ;
           for ( j = 1; j <= m/2 ; j++)
           {
               if ( rel_node < min_node + (j * (m/2)))
               {
                   //calculates time
                   tw_stime rate = rate_to_ns(bytes, core_bandwidth);
                    if(tw_now(lp) < ns->links[j-1].next_available_time)
                    {
                        printf("here4");
                        *time += (ns->links[j-1].next_available_time)- tw_now(lp);
                    }
                    else 
                    {
                        ns->links[j-1].next_available_time = tw_now(lp);
                    }   
                    *time += rate;
                    ns->links[j-1].next_available_time += rate;

                   //also flag doing send_down event
                   return codes_mapping_get_lpid_from_relative((ns->links[j-1].dest_node), lp_group_name,lp_type_name,ns->anno,0);
               }
           }
    }}
    else {
        //now we are at the topmost level and must send down

        int num_processing = (m/2) * switch_in_pod;
        int j ;
        int temp ;
        for ( j =1 ; j <= m; j ++)
        {
           if( rel_node < (num_processing * j)) 
           {
            tw_stime rate = rate_to_ns(bytes, core_bandwidth);
            if(tw_now(lp) < ns->links[j-1].next_available_time)
            {
                printf("here5");
                *time += (ns->links[j-1].next_available_time)- tw_now(lp);
            }
            else 
            {
                ns->links[j-1].next_available_time = tw_now(lp);
            }   
            *time += rate;
            ns->links[j-1].next_available_time += rate;
            return codes_mapping_get_lpid_from_relative((ns->links[j-1].dest_node),lp_group_name, lp_type_name, ns->anno, 0);
           }
        }
    }


        //add some sort of more sophisticated checking mechanism
//        return codes_mapping_get_lpid_from_relative((ns->links)[0].dest_node, lp_group_name, lp_type_name, ns->anno, 0);
  //  return codes_mapping_get_lpid_from_relative((ns->upLinks)[0].dest_node, lp_group_name,lp_type_name,ns->anno,0);
}
static void ft_switch_event(
        switch_state *s,
        tw_bf *b,
        ft_message *m,
        tw_lp *lp)
{
    switch (m->type)
    {
        case S_send_up:
            handle_S_send_up(s, b ,m ,lp);
            break;
        case S_send_down:
            handle_S_send_down(s , b ,m ,lp);
            break;
        default: 
            assert(0);
            break;
    }
  return;
}

static void ft_event(
    ft_state * ns,
    tw_bf * b,
    ft_message * m,
    tw_lp * lp)
    {
   switch (m->type)
    {
        case send_up:
            handle_send_up(ns, b, m, lp);
            break;
        case send_down:
            handle_send_down(ns, b, m, lp);
            break;
        default:
            assert(0);
            break;
    }
   return;
}
static void ft_switch_rev_event(
        switch_state * s,
        tw_bf *b,
        ft_message *m,
        tw_lp * lp)
{
    switch (m->type)
    {
        case send_up:
            handle_S_send_up_rev(s, b, m, lp);
            break;
        case send_down:
            handle_S_send_down_rev(s, b, m, lp);
            break;
        default:
            assert(0);
            break;
    }

    return;
}

    
static void ft_rev_event(
    ft_state * ns,
    tw_bf * b,
    ft_message * m,
    tw_lp * lp)
{

    switch (m->type)
    {
        case send_up:
            handle_send_up_rev(ns, b, m, lp);
            break;
        case send_down:
            handle_send_down_rev(ns, b, m, lp);
            break;
        default:
            assert(0);
            break;
    }

    return;
}

static void ft_finalize(
    ft_state * ns,
    tw_lp * lp)
{
    model_net_print_stats(lp->gid, &ns->ft_stats_array[0]);
    return;
}

int ft_get_magic()
{
  return ft_magic;
}

static tw_stime rate_to_ns(uint64_t bytes, double MB_p_s)
{
    tw_stime time;

    time = ((double)bytes)/(1024.0*1024.0);
    time = time / MB_p_s;
    time = time * 1000.0 * 1000.0 * 1000.0;
    return(time);
}
 void handle_S_send_down_rev(
    ft_state * ns,
    tw_bf * b,
    sn_message * m,
    tw_lp * lp)
{
    struct mn_stats* stat;

    (ns->links)[0].next_available_time = m->net_recv_next_idle_saved;
    
    stat = model_net_find_stats(m->category, ns->ft_stats_array);
    stat->recv_count--;
    stat->recv_bytes -= m->net_msg_size_bytes;
    stat->recv_time = m->recv_time_saved;

}

 void handle_send_down_rev(
    ft_state * ns,
    tw_bf * b,
    sn_message * m,
    tw_lp * lp)
{
    struct mn_stats* stat;

    (ns->links)[0].next_available_time = m->net_recv_next_idle_saved;
    
    stat = model_net_find_stats(m->category, ns->ft_stats_array);
    stat->recv_count--;
    stat->recv_bytes -= m->net_msg_size_bytes;
    stat->recv_time = m->recv_time_saved;

}
static void handle_S_send_down(
    switch_state * ns,
    tw_bf * b,
    ft_message * m,
    tw_lp * lp)
{   
    tw_event *e_new;
    ft_message *m_new;
    
    int dummy;
    tw_stime recv_queue_time = 0;
    struct mn_stats* stat;
    stat = model_net_find_stats(m->category, ns->ft_stats_array);
    stat->recv_count++;
   // printf("TEST%d", m->final_dest_gid);
    stat->recv_bytes += m->net_msg_size_bytes;
    m->recv_time_saved = stat->recv_time;
    codes_mapping_get_lp_info( lp->gid, lp_group_name,&dummy,lp_type_name,&dummy, ns->anno, &mapping_rep, &mapping_offset);

    /*
    stat->recv_time += rate_to_ns(m->net_msg_size_bytes,
            ns->params.net_bw_mbps);
    recv_queue_time += rate_to_ns(m->net_msg_size_bytes,
            ns->params.net_bw_mbps);
            */
    //will need to add state saving component and accurate time increases
    tw_lpid next_stop;
    if ( (ns->params.routing) == 0)
    {
        next_stop = get_down_stop(ns,lp, m->final_dest_gid, &recv_queue_time, m->net_msg_size_bytes);
    }
    else 
    {
        next_stop= get_next_down_ecmp( ns, lp, m->final_dest_gid, &recv_queue_time, m->net_msg_size_bytes);
    }
   // printf(" recv_queue_time %f", tw_now(lp)+recv_queue_time);
  //  printf(" check %f", recv_queue_time);
      //  printf("Next_stop: %d  LP_Dest %d", codes_mapping_get_lp_relative_id(next_stop,0,1), codes_mapping_get_lp_relative_id(m->final_dest_gid,0,1));
    //includes queing effect
    stat->recv_time += recv_queue_time;
   if (next_stop == m->final_dest_gid )
    {

         if(m->event_size_bytes)
        {
       
      //  void * m_n;
      /*
        ft_message * m_n;
        void *tmp_ptr = model_net_method_get_edata(FATTREE,m);
  //      int net_id model_net_get_id(LP_METHOD_NM);
//        e_new = model_net_event(net_id,m->category,m->sender_lp,0,recv_queue_time,m->event_size_bytes,tmp_ptr,0,NULL,lp);
            //codes_mapping_get_lpid_from_relative(next_stop,lp_group_name,"modelnet_fattree",ns->anno,0), tw_now(lp) + recv_queue_time,lp, FATTREE, (void **)&m_new, &m_data);
        //  e_new = tw_event_new(m->final_dest_gid, tw_now(lp) + recv_queue_time, lp);
//        e_new = model_net_method_event_new(m->final_dest_gid, recv_queue_time + tw_now(lp), lp, FATTREE,(void **)&m_new,&m_data);
        e_new = tw_event_new(m->final_dest_gid, tw_now(lp) + recv_queue_time,lp);
        m_new = tw_event_data(e_new);
         memcpy(m_new , m->temp_event, m->event_size_bytes);
  
      //      memcpy(m_new,m+1,m->event_size_bytes);
//          memcpy(m_data, tmp_ptr, m->event_size_bytes);
        
          tw_event_send(e_new);
          printf("--");
        printf("%dstuff ", m->event_size_bytes);
        printf(" --");
       }
      */
//        printf("GID: %d", next_stop);       
        void * m_data;

     //   printf("GID : %d", next_stop);   
//       e_new = model_net_method_event_new(m->final_dest_gid, tw_now(lp) + recv_queue_time,lp, FATTREE, (void **)&m_new, &m_data);
        e_new =tw_event_new(m->final_dest_gid,recv_queue_time, lp);
        m_new = tw_event_data(e_new);
        memcpy(m_new , m+1 , m->event_size_bytes);
          // memcpy(m_data, m+1 , m->event_size_bytes);
        //  m_new->type = send_down;
           tw_event_send(e_new); 
           return;
        
    }}
    else{
     e_new = tw_event_new(next_stop ,recv_queue_time, lp);
    m_new = tw_event_data(e_new);
  //  printf("recv_queue_time %f", recv_queue_time);
    memcpy(m_new, m, sizeof(ft_message));
    if(m->event_size_bytes)
    {
        memcpy(m_new + 1, m+1, m->event_size_bytes);
      //   memcpy(m_new->temp_event,m->temp_event,m->event_size_bytes);

    }
    m_new->type = S_send_down;
    tw_event_send(e_new);
    return;
    }
}
static void handle_send_down(
    ft_state * ns,
    tw_bf * b,
    ft_message * m,
    tw_lp * lp)
{   
    printf("first");
    tw_event *e_new;
    ft_message *m_new;
    tw_stime recv_queue_time = 0;
    struct mn_stats* stat;
    stat = model_net_find_stats(m->category, ns->ft_stats_array);
    stat->recv_count++;
    stat->recv_bytes += m->net_msg_size_bytes;
    m->recv_time_saved = stat->recv_time;
    void * tmp_ptr = model_net_method_get_edata(FATTREE,m);
    e_new = tw_event_new(m->final_dest_gid, tw_now(lp),lp);
    m_new = tw_event_data(e_new);
    memcpy(m_new ,tmp_ptr, m->event_size_bytes);
    tw_event_send(e_new);
    return;
}

 void handle_S_send_up_rev(
    ft_state * ns,
    tw_bf * b,
    sn_message * m,
    tw_lp * lp)
{
    (ns->upLinks)[0].next_available_time = m->net_send_next_idle_saved;

    codes_local_latency_reverse(lp);

    if(m->local_event_size_bytes > 0)
    {
        codes_local_latency_reverse(lp);
    }

    mn_stats* stat;
    stat = model_net_find_stats(m->category, ns->ft_stats_array);
    stat->send_count--;
    stat->send_bytes -= m->net_msg_size_bytes;
    stat->send_time = m->send_time_saved;

    return;
}
/* reverse computation for msg start event */
 void handle_send_up_rev(
    ft_state * ns,
    tw_bf * b,
    sn_message * m,
    tw_lp * lp)
{
    (ns->upLinks)[0].next_available_time = m->net_send_next_idle_saved;

    codes_local_latency_reverse(lp);

    if(m->local_event_size_bytes > 0)
    {
        codes_local_latency_reverse(lp);
    }

    mn_stats* stat;
    stat = model_net_find_stats(m->category, ns->ft_stats_array);
    stat->send_count--;
    stat->send_bytes -= m->net_msg_size_bytes;
    stat->send_time = m->send_time_saved;

    return;
}
static void handle_S_send_up(
    switch_state * ns,
    tw_bf * b,
    ft_message * m,
    tw_lp * lp)
{

    
    tw_event *e_new;
    ft_message *m_new;
    mn_stats* stat;
    int mapping_rep_id, mapping_offset, dummy;
    tw_lpid dest_id;
    char lp_group_name[MAX_NAME_LENGTH];
    int total_event_size;
    tw_stime send_queue_time = 0;
    total_event_size = model_net_get_msg_sz(FATTREE) + m->event_size_bytes;
    stat = model_net_find_stats(m->category, ns->ft_stats_array);
    stat->send_count++;
    stat->send_bytes += m->net_msg_size_bytes;
    m->send_time_saved = stat->send_time;
    send_queue_time = ns->params.net_startup_ns;
    int flag = 0;
    tw_lpid next_stop;
    if ((ns->params.routing) == 0)
    {
    next_stop= get_stop(ns,lp, m->final_dest_gid, &send_queue_time, m->net_msg_size_bytes);
    }
    else 
    {
        next_stop = get_next_up_ecmp(ns, lp, m->final_dest_gid, &send_queue_time, m->net_msg_size_bytes, &flag);
    }
    
    // may need to consider better way to determine
    /*
    printf("NODE ID %d ", codes_mapping_get_lp_relative_id(lp->gid,1,0));
    printf(" SEND TO ->>" );
    printf(" %d",codes_mapping_get_lp_relative_id(next_stop,1,0));
    */
//    if(m->final_dest_gid ==2) printf(" next %d", next_stop);
    e_new = tw_event_new(next_stop,send_queue_time, lp);
  //  printf(" send queue %f",tw_now(lp) +  send_queue_time);
  //  printf(" check %f", send_queue_time);
    m_new = tw_event_data(e_new);
//    if(lp->gid==6)printf("relative id %d", codes_mapping_get_lp_relative_id(lp->gid, 1, 0));
    //printf("NEXT_STOP %d", next_stop);
    memcpy(m_new, m, sizeof(ft_message));
  //  if(m->final_dest_gid == 3) printf("VERIFY %d", codes_mapping_get_lp_relative_id(next_stop,0,1));
    if(m->event_size_bytes)
    {

        memcpy(m_new + 1, m+1, m->event_size_bytes);
       // memcpy(m_new->temp_event,m->temp_event,m->event_size_bytes);

    }
    if (flag == 0) 
    {
  //      printf("!");
        m_new->type = S_send_up;
    }
    else
    {

        m_new->type = S_send_down;
    }
    tw_event_send(e_new);
    return;
}

/* handler for msg start event; this indicates that the caller is trying to
 * transmit a message through this NIC
 */
static void handle_send_up(
    ft_state * ns,
    tw_bf * b,
    ft_message * m,
    tw_lp * lp)
{

    tw_event *e_new;
    ft_message *m_new;
    mn_stats* stat;
    int mapping_rep_id, mapping_offset, dummy;
    tw_lpid dest_id;
    char lp_group_name[MAX_NAME_LENGTH];
    int total_event_size;
    tw_stime send_queue_time = 0;
    total_event_size = model_net_get_msg_sz(FATTREE) + m->event_size_bytes;
    stat = model_net_find_stats(m->category, ns->ft_stats_array);
    stat->send_count++;
    stat->send_bytes += m->net_msg_size_bytes;
    m->send_time_saved = stat->send_time;
      /* calculate send time stamp */
    if ( tw_now(lp) < (ns->upLinks)[0].next_available_time) 
        send_queue_time += (ns->upLinks)[0].next_available_time - tw_now(lp);
    else
        (ns->upLinks)[0].next_available_time = tw_now(lp);
   // will need to change later as there is not only one upLink
    m->next_available_time_saved = (ns->upLinks)[0].next_available_time;
    tw_lpid next_stop;
    codes_mapping_get_lp_info( lp->gid, lp_group_name,&dummy,lp_type_name,&dummy, ns->anno, &mapping_rep, &mapping_offset);
    next_stop=codes_mapping_get_lpid_from_relative((ns->upLinks)[0].dest_node, lp_group_name, "fattree_switch", NULL ,0 );
    
    send_queue_time += rate_to_ns(m->net_msg_size_bytes, ns->params.edge_bandwidth);
  //  printf("SEND %f", send_queue_time + tw_now(lp));
    (ns->upLinks)[0].next_available_time +=rate_to_ns(m->net_msg_size_bytes,ns->params.edge_bandwidth);
    e_new = tw_event_new(next_stop,send_queue_time,lp);
    m_new = tw_event_data(e_new);
    memcpy(m_new, m, sizeof(ft_message));
    if(m->event_size_bytes){
        memcpy(m_new + 1 ,model_net_method_get_edata(FATTREE,m),m->event_size_bytes);
    //    memcpy(m_new->temp_event,m->temp_event,m->event_size_bytes);
    }
    m_new->type= S_send_up;
    tw_event_send(e_new);
    model_net_method_idle_event(codes_local_latency(lp)+send_queue_time ,0,lp);
    
    return;
}

static tw_stime fattree_packet_event(
		char* category,
		tw_lpid final_dest_lp,
		uint64_t packet_size,
                int is_pull,
                uint64_t pull_size, /* only used when is_pull == 1 */
                tw_stime offset,
                const mn_sched_params *sched_params,
		int remote_event_size,
		const void* remote_event,
		int self_event_size,
		const void* self_event,
                tw_lpid src_lp,
		tw_lp *sender,
		int is_last_pckt)
{
//    printf("LP2 %d" , src_lp);       
     tw_event * e_new;
     tw_stime xfer_to_nic_time;
     ft_message * msg;
     char* tmp_ptr;
   

     xfer_to_nic_time = codes_local_latency(sender);
     e_new = model_net_method_event_new(sender->gid, xfer_to_nic_time+offset,
             sender, FATTREE, (void**)&msg, (void**)&tmp_ptr);
     msg->final_dest_gid = final_dest_lp;
     msg->src_gid = src_lp;
//     printf("%d", packet_size);
     msg->net_msg_size_bytes = packet_size;
     msg->event_size_bytes = 0;
     msg->local_event_size_bytes=0;
     msg->type = send_up;
     msg->magic = ft_get_magic();
    // printf("TEST %d", final_dest_lp);
     strcpy(msg->category,category);
   if(is_last_pckt){
     if(remote_event_size > 0)
            {
             msg->event_size_bytes = remote_event_size;
            memcpy(tmp_ptr, remote_event, remote_event_size);
            tmp_ptr += remote_event_size;
         //   memcpy(msg->temp_event,remote_event,remote_event_size);
            }
     if(self_event_size > 0)
            {
              msg->local_event_size_bytes = self_event_size;
              memcpy(tmp_ptr, self_event, self_event_size);
              tmp_ptr += self_event_size;
            }
  }
     
     tw_event_send(e_new);
     return xfer_to_nic_time;
}

static void ft_configure()
{
    anno_map = codes_mapping_get_lp_anno_map(LP_CONFIG_NM);
    assert(anno_map);
    num_params = anno_map->num_annos + (anno_map->has_unanno_lp > 0);
    all_params = malloc(num_params * sizeof(*all_params));
    for (uint64_t i = 0; i < anno_map->num_annos; i++){
        const char * anno = anno_map->annotations[i];
        int rc;
        rc = configuration_get_value_double(&config, "PARAMS",
                "net_startup_ns", anno, &all_params[i].net_startup_ns);
        if (rc != 0){
            tw_error(TW_LOC,
                    "simplenet: unable to read PARAMS:net_startup_ns@%s",
                    anno);
        }
        rc = configuration_get_value_double(&config, "PARAMS", "net_bw_mbps",
                anno, &all_params[i].net_bw_mbps);
        if (rc != 0){
            tw_error(TW_LOC, "simplenet: unable to read PARAMS:net_bw_mbps@%s",
                    anno);
        }
         rc = configuration_get_value_int(&config, "PARAMS",
                "m", anno, &all_params[i].m);
        if (rc != 0){
            tw_error(TW_LOC,
                    "simplenet: unable to read PARAMS:net_startup_ns@%s",
                    anno);
        }
        rc = configuration_get_value_int(&config, "PARAMS", "n",
                anno, &all_params[i].n);
        if (rc != 0){
            tw_error(TW_LOC, "simplenet: unable to read PARAMS:net_bw_mbps@%s",
                    anno);
        }
         rc = configuration_get_value_int(&config, "PARAMS",
                "edge_bandwidth", anno, &all_params[i].edge_bandwidth);
        if (rc != 0){
            tw_error(TW_LOC,
                    "simplenet: unable to read PARAMS:net_startup_ns@%s",
                    anno);
        }
        rc = configuration_get_value_int(&config, "PARAMS", "core_bandwidth",
                anno, &all_params[i].core_bandwidth);
        if (rc != 0){
            tw_error(TW_LOC, "simplenet: unable to read PARAMS:net_bw_mbps@%s",
                    anno);
        }



    }
    if (anno_map->has_unanno_lp > 0){
        int rc;
        rc = configuration_get_value_int(&config, "PARAMS",
                "net_startup_ns", NULL,
                &all_params[num_params-1].net_startup_ns);
        if (rc != 0){
            tw_error(TW_LOC, "simplenet: unable to read PARAMS:net_startup_ns");
        }
        rc = configuration_get_value_int(&config, "PARAMS", "net_bw_mbps",
                NULL, &all_params[num_params-1].net_bw_mbps);
        if (rc != 0){
            tw_error(TW_LOC, "simplenet: unable to read PARAMS:net_bw_mbps");
        }
        rc = configuration_get_value_int(&config, "PARAMS",
                "m", NULL,
                &all_params[num_params-1].m);
        if (rc != 0){
            tw_error(TW_LOC, "simplenet: unable to read PARAMS:net_startup_ns");
        }
        rc = configuration_get_value_int(&config, "PARAMS", "n",
                NULL, &all_params[num_params-1].n);
        if (rc != 0){
            tw_error(TW_LOC, "simplenet: unable to read PARAMS:net_bw_mbps");
        }
        rc = configuration_get_value_int(&config, "PARAMS",
                "edge_bandwidth", NULL,
                &all_params[num_params-1].edge_bandwidth);
        if (rc != 0){
            tw_error(TW_LOC, "simplenet: unable to read PARAMS:net_startup_ns");
        }
        rc = configuration_get_value_int(&config, "PARAMS", "core_bandwidth",
                NULL, &all_params[num_params-1].core_bandwidth);
        if (rc != 0){
            tw_error(TW_LOC, "simplenet: unable to read PARAMS:net_bw_mbps");
        }
        rc = configuration_get_value_int(&config, "PARAMS", "routing", NULL, &all_params[num_params-1].routing);
        if (rc != 0){
            tw_error(TW_LOC, "fattree:unable to read PARAMS:net_bw_mbps");
        }
    }
    //derived params
    int m = all_params[num_params-1].m;
    int n = all_params[num_params-1].n;
    all_params[num_params-1].row_num = 2 * power(n-1,m/2);
    all_params[num_params-1].total =((2*n)-1)*power(n-1,m/2);
    all_params[num_params-1].num_cores=all_params[num_params-1].total - (all_params[num_params-1].row_num)*(n -1);
    int row_num = all_params[num_params-1].row_num;
    all_params[num_params-1].switch_in_pod = row_num/m;

    int num_cores = all_params[num_params-1].num_cores;
    all_params[num_params-1].num_switches= num_cores + ((n-1)* row_num);

}


static tw_lpid ft_find_local_device(
        const char * annotation, 
        int          ignore_annotations,
        tw_lp      * sender)
{
     char lp_group_name[MAX_NAME_LENGTH];
     int mapping_rep_id, mapping_offset, dummy;
     tw_lpid dest_id;

     // TODO: don't ignore annotations
     codes_mapping_get_lp_info(sender->gid, lp_group_name, &dummy, NULL,
             &dummy, NULL, &mapping_rep_id, &mapping_offset);
     codes_mapping_get_lp_id(lp_group_name, LP_CONFIG_NM, annotation,
             ignore_annotations, mapping_rep_id, mapping_offset, &dest_id);

    return(dest_id);
}
// start of the code that will be used for seperate lps


#if SIMPLENET_DEBUG
void print_msg(sn_message *m){
    printf(" sn:\n  type:%d, magic:%d, src:%lu, dest:%lu, esize:%d, lsize:%d\n",
            m->event_type, m->magic, m->src_gid, m->final_dest_gid,
            m->event_size_bytes, m->local_event_size_bytes);
}
#endif


/*
 * Local variables:
 *  c-indent-level: 4
 *  c-basic-offset: 4
 * End:
 *
 * vim: ft=c ts=8 sts=4 sw=4 expandtab
 */
