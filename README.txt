### FatTreeSim README ###
A large-scale fattree simulator designed to simulate large-scale datacenter and HPC networks with accuracy. 

Fattree code located at: src/models/networks/model-net
### Dependencies ###
Codes-net; codes-base; MPI
### Building ###
0 - Checkout, build, and install codes-base

    git clone git@git.mcs.anl.gov:radix/codes-base
    <see codes-base/README.txt>

1 - If this is the first time you are building codes-net, run

    ./prepare.sh

2- Configure codes-net. This can be done in the source directory or in a
   dedicated build directory if you prefer out-of-tree builds.  The CC
   environment variable must refer to an MPI compiler.

    ./configure --with-codes-base=/path/to/codes-base/install --prefix=/path/to/codes-net/install CC=mpicc

3 - Build codes-net

    make clean && make
    make install
    make tests

4 - (optional) run test programs

    make check

### Running ###
* Can run in three modes sequential, conservative, and optimistic. The latter two use MPI.
* Sample sequential run
* * ./tests/modelnet-test --sync=1 tests/conf/modelnet-test-fattree.conf
* Sample conservative run
* * mpiexec -n 8 ./tests/modelnet-test --sync=2 tests/conf/modelnet-test-fattree.conf